<?php
declare(strict_types=1);
class NameGenerator
{
    private static $instance = null;
    private $generatedNames = [];
    public static function getOrNew()
    {
        if (static::$instance == null)
            static::$instance = new static;
        return static::$instance;
    }
    public function uniqueName()
    {
        $name = $this->generateName();
        while (isset($this->generatedNames[$name])) {
            $name = $this->generateName();
        }
        $this->generatedNames[$name] = true;
        return $name;
    }
    private function generateName(): string
    {
        return chr(rand(65,90)) . chr(rand(65,90)) . rand(100, 999);
    }
}
$generatedNames = [];
class Robot
{
    public $name;
    public function __construct()
    {
        $this->setName();
    }
    public function getName(): string
    {
        return $this->name;
    }
    public function reset(): void
    {
        $this->setName();
    }
    private function setName()
    {
        $this->name = NameGenerator::getOrNew()->uniqueName();
    }
}

?>