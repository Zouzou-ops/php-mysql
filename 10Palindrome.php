<?php

declare(strict_types=1);
function isPalindrome(int $number): bool
{
   $text = "{$number}";
   if (strlen($text) == 1) {
      return true;
   }
   while(strlen($text) > 1) {
      if(substr($text, 0, 1) != substr($text, -1)) {
         return false;
      }
      $text = substr($text, 1, -1);
   }
   return true;
}
function addFactor(array &$factors, $i, $j): void
{
   $factor = [$i < $j ? $i : $j, $i < $j ? $j : $i];
   if (in_array($factor, $factors)) {
      return;
   }
   $factors[] = $factor;
}
function smallest(int $min, int $max): array
{
    $facts = [];
    $first = 0;
    for($i = $min; $i <= $max; $i++) {
        for($j = $min; $j <= $max; $j++) {
           $test = $i * $j;
           if ($first && $test > $first) {
               break;
           }
           if (!isPalindrome($test)) {
               continue;
           }
           if ($first == 0) {
               $first = $test;
               addFactor($facts, $i, $j);
               continue;
           }
           if ($test < $first) {
               $facts = [];
               $first = $test;
               continue;
            }
           addFactor($facts, $i, $j);
        }
    }
    if (!count($facts)) {
        throw new Exception();
    }
    return [$first, $facts];
}
function largest(int $min, int $max): array
{
    $facts = [];
    $first = 0;
    for($i = $max; $i >= $min; $i--) {
        for($j = $max; $j >= $min; $j--) {
           $test = $i * $j;
           if ($test < $first) {
               break;
           }
           if (!isPalindrome($test)) {
               continue;
           }
           if ($first == 0) {
               $first = $test;
               addFactor($facts, $i, $j);
               continue;
           }
           if ($test > $first) {
               $facts = [];
               $first = $test;
               continue;
            }
           addFactor($facts, $i, $j);
        }
    }
    if (!count($facts)) {
        throw new Exception();
    }
    return [$first, $facts];
}
?>