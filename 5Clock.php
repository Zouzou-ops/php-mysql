<?php
declare(strict_types=1);
class Clock
{
    public $date = '';
    public function __construct($hours, $minute = null)
    {
        /* var_dump($hours, $minute); */
        $this->date = new DateTime();
        $day =$this->date->format('d');
        $month = $this->date->format('m');
        $year = $this->date->format('Y');
        // rule 1
        if ($hours > 0 && $hours <= 23 &&  $minute > 0 && $minute < 59){
            $this->date->setTime($hours, $minute);
            return;
        }
        $this->date->setTime(24, 00);
        if ($hours < 0) {
            $hours = abs($hours);
            $this->date->sub(new DateInterval("PT{$hours}H"));
        } else {
            echo 'add : ' . $hours . PHP_EOL; 
            $this->date->add(new DateInterval("PT{$hours}H"));
        }
        echo 'H : ' . $this->date->format('H') .PHP_EOL; 
        if ($minute){
            if ($minute < 0) {
                $minute = abs($minute);
                $this->date->sub(new DateInterval("PT{$minute}M"));
            } else {
                $this->date->add(new DateInterval("PT{$minute}M"));
            }
            echo 'M : ' . $this->date->format('i') .PHP_EOL; 
        }
        $this->date->setDate((int)$year, (int)$month, (int)$day);
    }
    public function add($minute) 
    {
        if ($minute < 0) {
            $minute = abs($minute);
            $this->date->sub(new DateInterval("PT{$minute}M"));
            return $this;
        }
        $this->date->add(new DateInterval("PT{$minute}M"));
        return $this;
    }
    public function sub($minute) 
    {
        $this->date->sub(new DateInterval("PT{$minute}M"));
        return $this;
    }
    public function __toString(): string
    {
        return $this->date->format('H:i');
    }
}
?>