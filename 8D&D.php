<?php

declare(strict_types=1);
class DndCharacter
{    
    public function __construct()
    {
        $this->strength = $this->ability();
        $this->dexterity = $this->ability();
        $this->constitution = $this->ability();
        $this->intelligence = $this->ability();
        $this->wisdom = $this->ability();
        $this->charisma = $this->ability();
        $this->hitpoints = 10 + $this->modifier($this->constitution);
    }
    ?>