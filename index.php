<?php
declare(strict_types=1);
class Clock
{
    private $hours;
    private $minutes;
    
    public function __construct($hours = 0, $minutes = 0)
    {
        $totalMinutes = $hours * 60 + $minutes;
        
        list($this->hours, $this->minutes) = $this->normalizeClock($totalMinutes);
    }
    
    public function __toString(): string
    {
        return sprintf('%02d:%02d', $this->hours, $this->minutes);
    }
    
    public function add($minutes)
    {
        $totalMinutes = $this->hours * 60 + $this->minutes + $minutes;
        
        list($this->hours, $this->minutes) = $this->normalizeClock($totalMinutes);
        
        return $this;
    }
    
    public function sub($minutes)
    {
        $totalMinutes = $this->hours * 60 + $this->minutes - $minutes;
        
        list($this->hours, $this->minutes) = $this->normalizeClock($totalMinutes);
        
        return $this;
    }
    
    private function normalizeClock($totalMinutes)
    {
        if ($totalMinutes < 0) 
        {
            $totalMinutes = $totalMinutes % 1440;
            $totalMinutes = 1440 + $totalMinutes;
        }
        
        $hours = intval($totalMinutes / 60);
        $hours = $hours % 24;
        $minutes =$totalMinutes % 60;
        
        return [$hours, $minutes];
    }
}
?>