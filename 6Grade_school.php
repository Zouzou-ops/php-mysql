<?php
class School
{
    public function __construct() {
        $this->schoolList = [];
    }
    public function add(string $name, int $grade): void
    {
        if (!isset($this->schoolList[$grade])) {
            $this->schoolList[$grade] = [];
        }
        array_push($this->schoolList[$grade], $name);
    }
    public function grade($grade)
    {
        if (isset($this->schoolList[$grade])) {
            return $this->schoolList[$grade];
        } else {
            return [];
        }
    }
    public function studentsByGradeAlphabetical(): array
    {
        ksort($this->schoolList);
        foreach($this->schoolList as $key => $name) {
            sort($this->schoolList[$key]);
        }
        var_dump($this->schoolList);
        return $this->schoolList;
    }
}